<?php
/*
    Template Name: Careers
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <?php get_template_part('template-parts/components/hero', 'banner'); ?>
</div>

<main id="target" class="page-content contact-wrapper">
    <div class="page-content-tab trapezoid">Careers</div>

    <section class="container">
        <header class="career-header">
            <div>
                <h2><?php the_field( 'header_title' ); ?></h2>
            </div>
            <div class="career-writeup">
                <hr class="hr-bar-green">
                <p><?php the_field( 'header_text' ); ?></p>
            </div>
        </header>


        <div class="career-content">
            <div class="job-openings">
                <h3>Job Openings</h3>

                <div class="job-listings">
                    <?php get_template_part('template-parts/content', 'job'); ?>
                </div>
            </div>
        </div>
    </section>

    <div class="our-people-slider">
        <h2>Our People</h2>
        <div class="slider-container">
            <div id="our-people-flexslider" class="flexslider">
                <ul class="slides">

                    <?php
                        // WP_Query arguments
                        $args = array(
                            'post_type' => 'people',
                            'posts_per_page' => 3,
                        );

                        // The Query
                        $query_people = new WP_Query( $args );

                        // The Loop
                        if ( $query_people->have_posts() ) {
                            while ( $query_people->have_posts() ) {
                                $query_people->the_post(); ?>

                                <li>
                                    <div class="slide">
                                        <div class="slide-content">
                                            <p class="slide-quote"><?php the_field( 'excerpt' ); ?></p>
                                            <div class="slide-author">
                                                <hr class="hr-bar-green">
                                                <div class="author-name"><?php the_title(); ?></div>
                                                <div class="author-position"><?php the_field( 'position' ); ?></div>
                                            </div>
                                            <div class="slide-btn">
                                                <div>
                                                    <a href="<?php the_permalink(); ?>" class="lightbox-popup btn">Read More <?php get_template_part('img/rarr') ?></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="slide-image">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                                        </div>
                                    </div>
                                </li>

                            <?php }
                        } else { ?>
                            <p>No posts found.</p>
                        <?php }
                        // Restore original Post Data
                        wp_reset_postdata();
                    ?>

                </ul>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>