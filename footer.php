
<footer id="footer" class="site-footer">
    <div class="footer-boxes">
        <div class="footer-box">
            <?php if ( have_rows( 'address_+_map', 'option' ) ) : ?>
                <?php while ( have_rows( 'address_+_map', 'option' ) ) : the_row(); ?>
                    <h5><?php the_sub_field( 'title' ); ?></h5>
                    <p><?php the_sub_field( 'address' ); ?></p>
                    <a class="view-map" target="_blank" href="<?php the_sub_field( 'link_to_map' ); ?>">View Map</a>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <div class="footer-box">
            <table class="footer-contact">
                <tbody>
                    <?php if ( have_rows( 'contact_details', 'option' ) ) : ?>
                        <?php while ( have_rows( 'contact_details', 'option' ) ) : the_row(); ?>
                            <?php if ( have_rows( 'contact_numbers' ) ) : ?>
                                <?php while ( have_rows( 'contact_numbers' ) ) : the_row(); ?>

                                    <tr>
                                        <td><?php the_sub_field( 'title' ); ?></td>
                                        <td><a href="tel:<?php the_sub_field( 'number' ); ?>"><?php the_sub_field( 'number' ); ?></a></td>
                                    </tr>

                                <?php endwhile; ?>
                            <?php else : ?>
                                <?php // no rows found ?>
                            <?php endif; ?>
                            <?php if ( have_rows( 'email_addresses' ) ) : ?>
                                <?php while ( have_rows( 'email_addresses' ) ) : the_row(); ?>
                                    <tr class="footer-email">
                                        <td><?php the_sub_field( 'title' ); ?></td>
                                        <td><a href="mailto:<?php the_sub_field( 'email_address' ); ?>"><?php the_sub_field( 'email_address' ); ?></a></td>
                                    </tr>


                                <?php endwhile; ?>
                            <?php else : ?>
                                <?php // no rows found ?>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

        <div class="footer-box">
            <nav class="footer-nav">
                <?php the_field('footer_menu', 'option'); ?>
            </nav>

            <div>
                <h6>Follow Us</h6>
                <ul class="social-links">
                    <?php if ( have_rows( 'social_links', 'option' ) ) : ?>
                        <?php while ( have_rows( 'social_links', 'option' ) ) : the_row(); ?>

                        <?php if( get_sub_field('twitter') ): ?>
                            <li><a href="<?php the_sub_field( 'twitter' ); ?>" target="_blank"><?php get_template_part('img/icon', 'twitter'); ?></a></li>
                        <?php endif; ?>

                        <?php if( get_sub_field('facebook') ): ?>
                            <li><a href="<?php the_sub_field( 'facebook' ); ?>" target="_blank"><?php get_template_part('img/icon', 'facebook'); ?></a></li>
                        <?php endif; ?>

                        <?php if( get_sub_field('linked_in') ): ?>
                            <li><a href="<?php the_sub_field( 'linked_in'); ?>" target="_blank"><?php get_template_part('img/icon', 'linkedin'); ?></a></li>
                        <?php endif; ?>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">
        &copy; <?php echo date('Y'); ?> TKL Steel Corp.
    </div>
</footer>

</div><!-- #site-wrapper -->

<?php wp_footer(); ?>

</body>
</html>
