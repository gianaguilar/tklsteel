<?php
/*
    Template Name: CSR Projects
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <?php get_template_part('template-parts/components/hero', 'banner'); ?>
</div>

<div id="target" class="contain-tabbar">
    <div id="tab-bar" class="tab-bar-menu">
        <div class="tab-bar-title trapezoid">Trust TKL</div>
        <nav>
            <ul>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/trust-tkl' ) ); ?>">The TKL Story</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/our-people' ) ); ?>">Our People</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/client-stories' ) ); ?>">Client Stories</a></li>
                <li><a class="tab-bar-link active" href="<?php echo esc_url( home_url( '/csr-projects' ) ); ?>">CSR Projects</a></li>
            </ul>
        </nav>
    </div>

    <main class="page-content">
        <?php get_template_part('template-parts/components/page', 'header'); ?>

        <section class="projects-container">
            <h2 class="projects-category">Our CSR <br>Projects</h2>
            <?php get_template_part('template-parts/content', 'csr'); ?>
        </section>
    </main>
</div>

<div class="continue-reading">
    <div class="container">
        <a href="<?php echo esc_url( home_url( '/steel-products' ) ); ?>" class="cr-link">
            <span class="link-text">
                <div>Continue Exploring</div>
                <h4>TKL Steel</h4>
            </span>

            <div class="cr-triangle"><img src="<?php echo esc_url( get_template_directory_uri()) . '/img/continue-triangle.svg';?>" alt="image description">
            <span class="cr-arrow"><?php get_template_part('img/rarr'); ?></span>
            </div>
        </a>
    </div>
</div>




<?php get_footer(); ?>