<?php
/*
    Template Name: Warehouse - Manila
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <header class="page-hero">
        <?php if (get_field('banner_type') == 'image') { ?>

            <div class="hero-banner">
                <img class="banner-image" src="<?php the_field( 'page_banner_image' ); ?>" />
                <a class="scroll-down" href="#target">Scroll Down</a>
            </div>

        <?php } else { ?>

            <div class="hero-banner">
                <ul class="slides">

                    <?php if ( have_rows( 'banner_slider' ) ) : ?>
                        <?php while ( have_rows( 'banner_slider' ) ) : the_row(); ?>
                            <li>
                                <?php if ( get_sub_field( 'image' ) ) { ?>
                                <img class="banner-image" src="<?php the_sub_field( 'image' ); ?>" />
                                <?php } ?>
                            </li>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>

                </ul>

                <a class="scroll-down" href="#target">Scroll Down</a>
            </div>

        <?php } ?>
    </header>
</div>

<div id="target" class="contain-tabbar">
    <div id="tab-bar" class="tab-bar-menu">
        <div class="tab-bar-title trapezoid">Warehouse</div>
        <nav>
            <ul>
                <li><a class="tab-bar-link active" href="<?php echo esc_url( home_url( '/warehouse-manila' ) ); ?>">Manila</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/warehouse-valenzuela' ) ); ?>">Valenzuela</a></li>
            </ul>
        </nav>
    </div>


    <main id="warehouse-container" class="page-content warehouse">
        <?php if (function_exists('ssf_wp_template')) {print ssf_wp_template('[SUPER-STORE-FINDER CAT=Manila]');} ?>
    </main>
</div>

<?php get_footer(); ?>