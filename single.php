<?php get_header(); ?>

<main class="page-content">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article class="container">
        <header class="single-article article-header">
            <?php if(has_category()) { ?>
                <div class="single-category">
                    <?php
                    $category = get_the_category();
                    echo $category[0]->cat_name;
                    ?>
                </div>
            <?php } ?>
            <h2 class="single-title"><?php the_title(); ?></h2>
            <div class="single-meta">
                <span>by <?php the_author(); ?></span>
                <span><?php the_time('F j, Y'); ?></span>
            </div>
        </header>

        <div class="single-content">
            <?php the_content (); ?>
        </div>
    </article>
    <?php endwhile; ?>
    <!-- post navigation -->
    <?php else: ?>
    <!-- no posts found -->
    <?php endif; ?>
</main>

<?php get_footer(); ?>