<?php get_header(); ?>
<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
</div>


<div>
    <main class="page-content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article class="container">
                <header class="single-article article-header">
                    <?php if(has_category()) { ?>
                        <div class="single-category">
                            <?php
                            $category = get_the_category();
                            echo $category[0]->cat_name;
                            ?>
                        </div>
                    <?php } ?>
                    <h2 class="single-title"><?php the_title(); ?></h2>
                    <div class="single-meta">
                        <span>by <?php the_author(); ?></span>
                        <span><?php the_time('F j, Y'); ?></span>
                    </div>
                </header>

                <div class="single-content article-content">
                    <?php the_content (); ?>
                </div>
            </article>

        <?php endwhile; ?>

            <div id="print-blog" class="container">
                <div class="share-bar no-print">
                    <ul>
                        <li id="print-me"><span class="print-button"><?php get_template_part('img/print', 'icon') ?>Print</span></li>

                        <?php if ( get_field( 'file_upload' ) ) { ?>
                            <li><a href="<?php the_field( 'file_upload' ); ?>" target="_blank" download><?php get_template_part('img/download', 'icon') ?>Download</a></li>
                        <?php } ?>

                        <li><a class="share-trigger"><?php get_template_part('img/share', 'icon') ?>Share</a></li>
                    </ul>

                    <?php get_template_part('template-parts/components/share', 'icons'); ?>
                </div>
            </div>

        <?php else: ?>

        <?php endif; ?>
    </main>
</div>



<?php get_footer(); ?>