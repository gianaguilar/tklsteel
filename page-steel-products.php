<?php
/*
    Template Name: Steel Products
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <?php get_template_part('template-parts/components/product', 'banner'); ?>
</div>

<div id="target" class="contain-tabbar tab-bar-container">
    <?php get_template_part('template-parts/components/product', 'tab-bar'); ?>

    <main class="page-content">
        <section class="container">
            <header class="product-header">
                <div>
                    <h2 id="product-title"><?php the_field( 'title' ); ?></h2>
                </div>
                <div>
                    <hr class="hr-bar-green">
                    <p><?php the_field( 'text' ); ?></p>
                </div>
            </header>

            <div class="products-index">
                <?php echo do_shortcode( '[ajax_load_more id="6649317813" preloaded="true" preloaded_amount="12" sticky_posts="true" repeater="template_7" post_type="product" posts_per_page="12" order="ASC" pause="true" scroll="false" transition="fade" transition_container="false" button_label="View More Products" button_loading_label="Loading..."]' ); ?>


                <div class="filler"></div>
                <div class="filler"></div>
                <div class="filler"></div>
                <div class="filler"></div>
            </div><!-- /.products-index -->

        </section>
    </main>
</div>

<?php get_footer(); ?>