<?php $post = get_post($_POST['id']); ?>

<div id="post-<?php the_ID(); ?>" class="lightbox-post lb-people">

    <?php while (have_posts()) : the_post(); ?>

        <header class="lightbox-header">
            <div class="lightbox-header-inner">
                <div>
                    <h2 class="lightbox-title"><?php the_title();?></h2>
                </div>
                <div>
                    <div class="lightbox-position"><?php the_field( 'position' ); ?></div>
                    <div class="lightbox-detail"><?php the_field( 'details' ); ?></div>
                </div>
            </div>
        </header>

        <article class="lightbox-article">
            <div class="lb-content-left">
                <?php get_template_part('template-parts/content-lb', 'gallery') ?>
                <p class="lb-excerpt"><?php the_field( 'excerpt' ); ?></p>
            </div>

            <div class="lb-content-right">
                <div class="lb-content">
                    <?php the_content();?>
                </div>
                <br>
                <?php get_template_part('template-parts/components/share', 'bar'); ?>
            </div>

        </article>

    <?php endwhile;?>

</div>