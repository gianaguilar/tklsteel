<?php $post = get_post($_POST['id']); ?>

<div id="post-<?php the_ID(); ?>" class="lightbox-post lb-job">

    <?php while (have_posts()) : the_post(); ?>

        <header id="target-header" class="lightbox-header">
            <div>
                <h2 class="lightbox-title"><?php the_title();?></h2>
            </div>
        </header>

        <div class="job-content">
            <article class="lightbox-article">
                <div class="lb-content-left">
                    <h3 class="job-heading">Job Description</h3>
                    <?php the_field( 'job_description' ); ?>
                </div>

                <div class="lb-content-right">
                    <h3 class="job-heading">Requirements</h3>
                    <?php the_field( 'requirements' ); ?>
                </div>
            </article>

            <footer class="job-footer">
                <div>
                    <a class="btn apply-now" href="#target-header">Apply Now <?php get_template_part('img/rarr' ); ?></a>
                </div>
                <div>
                    <?php get_template_part('template-parts/components/share', 'bar'); ?>
                </div>
            </footer>
        </div>

        <div class="application-content isHidden">
            <article class="lightbox-article">
                <div class="lb-content-left">
                    <p>Thank you for your interest in joining our growing organization.</p>
                    <p>You may click the link below and email us your job application.</p>

                    <a class="apply-email" href="mailto:careers@tklsteelcorp.com.ph"><?php get_template_part('img/icon', 'mail-2'); ?>careers@tklsteelcorp.com.ph</a>

                </div>

                <div class="lb-content-right">
                    <h3 class="job-heading">Important Reminders</h3>
                    <ol>
                        <li>
                            Put your name and the position you applying for in the subject of your email message. <br><br>
                            example:<br><br>
                            (Subject)<br>
                            <strong>Antonio Dela Cruz, Truck Driver</strong><br><br>
                        </li>
                        <li>Attach your latest resume file. You may use MS Word or PDF format.</li>
                    </ol>
                </div>
            </article>

            <footer class="job-footer">
                <div>
                    <a class="btn apply-now-back" href="#target-header"><?php get_template_part('img/larr' ); ?>Back</a>
                </div>
            </footer>
        </div>

    <?php endwhile;?>

</div>