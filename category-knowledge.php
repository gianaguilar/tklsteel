<?php get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <header class="page-hero">
        <div class="hero-banner">
            <ul class="slides">
                <?php
                    // WP_Query arguments
                    $args = array(
                        'post_type' => 'blog-posts',
                        'category_name' => 'knowledge',
                        'posts_per_page' => 3,
                    );

                    // The Query
                    $query_innovations = new WP_Query( $args );

                    // The Loop
                    if ( $query_innovations->have_posts() ) {
                        while ( $query_innovations->have_posts() ) {
                            $query_innovations->the_post(); ?>

                            <li class="full-bg blog-slide" style="background-image: url('<?php the_post_thumbnail_url(); ?> ')" alt="slider image">

                                <div class="slide-article">
                                    <header class="folder-label <?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>">
                                        <h3><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></h3>
                                    </header>


                                    <div class="slide-details">
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <a class="btn-link" href="<?php the_permalink(); ?>">Read Story <?php get_template_part('img/rarr'); ?></a>
                                    </div>
                                </div>
                            </li>

                        <?php }
                    } else { ?>
                        <p>No posts found.</p>
                    <?php }
                    // Restore original Post Data
                    wp_reset_postdata();
                ?>

            </ul>
        </div>
    </header>
</div>

<div class="contain-tabbar">
    <div id="tab-bar" class="tab-bar-menu">
        <div class="tab-bar-title trapezoid"><a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">Blog</a></div>
        <nav>
            <ul>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/tkl-news' ) ); ?>">TKL News</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/innovations' ) ); ?>">Innovations</a></li>
                <li><a class="tab-bar-link active" href="<?php echo esc_url( home_url( '/category/knowledge' ) ); ?>">Knowledge</a></li>
            </ul>
        </nav>
    </div>

    <main class="page-content blog-content">
        <section class="container">
            <div class="blog-section knowledge">
                <header class="folder-label">
                    <h3>Knowledge</h3>
                </header>

                <?php echo do_shortcode( '[ajax_load_more cache="true" cache_id="6217044270" preloaded="true" preloaded_amount="6" repeater="template_5" post_type="blog-posts" posts_per_page="6" category="knowledge" pause="true" scroll="false" transition="fade" button_label="Load More News" button_loading_label="Loading"]' ); ?>
            </div>

        </section>
    </main>
</div>

<?php get_footer(); ?>