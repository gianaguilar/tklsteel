<?php
/*
    Taxonomy: Product Type
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <?php get_template_part('template-parts/components/product', 'banner'); ?>
</div>

<div class="contain-tabbar tab-bar-container">
    <?php get_template_part('template-parts/components/product', 'tab-bar'); ?>

    <main class="page-content">
        <section class="container">
            <header class="product-header">
                <div>
                    <?php
                        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                    ?>
                    <h2><?php echo $term->name; // will show the name ?></h2>
                </div>
                <div>
                    <hr class="hr-bar-green">
                    <p>We make sure our products fit the high standards of TKL because your business and your safety matter to us.</p>
                </div>
            </header>

            <div class="products-index">
                <?php
                    // Reference: http://bit.ly/2ffag5P
                    $terms = get_term_by( 'slug', get_query_var( 'term' ));
                    $term_slug = $term->slug;
                    $taxonomy = get_query_var( 'taxonomy' );

                    $tax = $taxonomy;
                    $tax_term = $term_slug;

                    // Reference: http://bit.ly/2ff0aSG
                    $shortcode = sprintf(
                        '[ajax_load_more id="6649317813" preloaded="true" preloaded_amount="12" repeater="template_7" post_type="product" taxonomy="%1$s" taxonomy_terms="%2$s" posts_per_page="12" order="ASC" orderby="title" pause="true" scroll="false" transition="fade" transition_container="false" button_label="View More Products" button_loading_label="Loading..."]',
                        $tax,
                        $tax_term
                    );
                    echo do_shortcode( $shortcode );
                ?>

                <div class="filler"></div>
                <div class="filler"></div>
                <div class="filler"></div>
                <div class="filler"></div>
            </div><!-- /.products-index -->

        </section>
    </main>
</div>
<?php get_footer(); ?>