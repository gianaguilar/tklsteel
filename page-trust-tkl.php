<?php
/*
    Template Name: Trust TKL
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <?php get_template_part('template-parts/components/hero', 'banner'); ?>
</div>

<div id="target" class="contain-tabbar">
    <div id="tab-bar" class="tab-bar-menu">
        <div class="tab-bar-title trapezoid"><?php the_title(); ?></div>
        <nav>
            <ul>
                <li><a class="tab-bar-link active" href="<?php echo esc_url( home_url( '/trust-tkl' ) ); ?>">The TKL Story</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/our-people' ) ); ?>">Our People</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/client-stories' ) ); ?>">Client Stories</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/csr-projects' ) ); ?>">CSR Projects</a></li>
            </ul>
        </nav>
    </div>

    <main class="page-content">
        <?php if ( have_rows( 'section_1' ) ) : ?>
            <?php while ( have_rows( 'section_1' ) ) : the_row(); ?>
                <section class="trust-matters container">
                    <h2><?php the_sub_field( 'title' ); ?></h2>
                    <div class="page-header">
                        <div>
                            <?php if ( get_sub_field( 'image' ) ) { ?>
                                <img src="<?php the_sub_field( 'image' ); ?>" />
                            <?php } ?>
                        </div>
                        <div>
                            <hr class="hr-bar-green">
                            <p><?php the_sub_field( 'text' ); ?></p>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
        <?php endif; ?>

        <?php if ( have_rows( 'section_2' ) ) : ?>
            <?php while ( have_rows( 'section_2' ) ) : the_row(); ?>
            <section class="strong-foundations">
                <div class="container">
                    <div class="page-header">

                        <div>
                            <?php the_sub_field( 'title' ); ?>
                        </div>

                        <div class="strong-foundations">
                            <hr class="hr-bar-white">
                            <p><?php the_sub_field( 'text' ); ?></p>
                        </div>

                    </div>
                </div>
            </section>
            <?php endwhile; ?>
        <?php endif; ?>


        <?php if ( have_rows( 'section_3' ) ) : ?>
            <?php while ( have_rows( 'section_3' ) ) : the_row(); ?>
            <section class="our-mission">
                <h2><?php the_sub_field( 'title' ); ?></h2>
                <div class="page-header">
                    <div class="our-mission-content">
                        <hr class="hr-bar-green">
                        <?php the_sub_field( 'text' ); ?>
                    </div>
                    <div class="our-mission-image">
                        <?php if ( get_sub_field( 'image' ) ) { ?>
                            <img src="<?php the_sub_field( 'image' ); ?>" />
                        <?php } ?>
                    </div>
                </div>
            </section>
            <?php endwhile; ?>
        <?php endif; ?>



        <?php if ( have_rows( 'section_4' ) ) : ?>
            <?php while ( have_rows( 'section_4' ) ) : the_row(); ?>
            <section class="our-vision">
                <div class="container">
                    <h2><?php the_sub_field( 'title' ); ?></h2>
                    <div class="our-vision-content">
                        <div>
                            <hr class="hr-bar-white">
                            <br>
                            <?php the_sub_field( 'text' ); ?>
                        </div>
                    </div>
                </div>
            </section>
            <?php endwhile; ?>
        <?php endif; ?>
    </main>
</div>

<div class="continue-reading">
    <div class="container">
        <a href="<?php echo esc_url( home_url( '/our-people' ) ); ?>" class="cr-link">
            <span class="link-text">
                <div>Continue Reading</div>
                <h4>Our People</h4>
            </span>

            <div class="cr-triangle"><img src="<?php echo esc_url( get_template_directory_uri()) . '/img/continue-triangle.svg';?>" alt="image description">
            <span class="cr-arrow"><?php get_template_part('img/rarr'); ?></span>
            </div>
        </a>
    </div>
</div>



<?php get_footer(); ?>