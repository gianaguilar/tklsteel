<?php
/**
 * TKL Steel functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package TKL_Steel
 */

if ( ! function_exists( 'tkl_steel_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tkl_steel_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on TKL Steel, use a find and replace
	 * to change 'tkl-steel' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'tkl-steel', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
        'menu-main' => esc_html__( 'Main Menu', 'tkl-steel' ),
        'menu-types' => esc_html__( 'Products Types', 'tkl-steel' ),
        'menu-usage' => esc_html__( 'Product Usage', 'tkl-steel' ),
		'menu-footer' => esc_html__( 'Footer Menu', 'tkl-steel' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tkl_steel_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 250,
		'width'       => 250,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'tkl_steel_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tkl_steel_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tkl_steel_content_width', 640 );
}
add_action( 'after_setup_theme', 'tkl_steel_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tkl_steel_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tkl-steel' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tkl-steel' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tkl_steel_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tkl_steel_scripts() {
	wp_enqueue_style( 'tkl-style', get_stylesheet_uri() );
    wp_enqueue_style( 'tkl-custom', get_template_directory_uri() . '/styles/css/app.css' );

    wp_enqueue_script('jquery');
	// wp_enqueue_script( 'tkl-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
    // wp_enqueue_script( 'tkl-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script( 'tkl-flexslider', get_template_directory_uri() . '/js/min/flexslider.js', array(), '20151215', true );
    wp_enqueue_script( 'tkl-magnific', get_template_directory_uri() . '/js/min/magnific.js', array(), '20151215', true );
    wp_enqueue_script( 'tkl-scrollreveal', get_template_directory_uri() . '/js/min/scrollreveal.js', array(), '20151215', true );
    wp_enqueue_script( 'tkl-print', get_template_directory_uri() . '/js/min/print.js', array(), '20151215', true );
    wp_enqueue_script( 'tkl-headroom', get_template_directory_uri() . '/js/min/headroom.js', array(), '20151215', true );
    wp_enqueue_script( 'tkl-app', get_template_directory_uri() . '/js/min/app-min.js', array(), '20151215', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tkl_steel_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


// Relevanssi
function my_alm_query_args_relevanssi($args){
   $args = apply_filters('alm_relevanssi', $args);
   return $args;
}
add_filter( 'alm_query_args_relevanssi', 'my_alm_query_args_relevanssi');


function display_taxonomy_terms($post_type, $display = false) {
    global $post;
    $term_list = wp_get_post_terms($post->ID, $post_type, array('fields' => 'names'));

    if($display == false) {
        echo $term_list[0];
    }elseif($display == 'return') {
        return  $term_list[0];
    }
}

// ACF Options Page
if ( function_exists('acf_add_options_page')) {
    acf_add_options_page();
    acf_add_options_sub_page('Global Options');
    // acf_add_options_sub_page('New Sub Page');
}