<?php get_header(); ?>
<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <header class="page-hero">
        <div class="hero-banner">
            <?php if ( has_post_thumbnail() ) { ?>

                <?php the_post_thumbnail('', array('class' => 'banner-image')); ?>

            <?php } else { ?>

                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/16x9.svg';?>" alt="No Image Found">

            <?php } ?>

            <a class="scroll-down" href="#target">Scroll Down</a>
        </div>
    </header>
</div>


<div id="target" class="contain-tabbar">
    <div id="tab-bar" class="tab-bar-menu">
        <div class="tab-bar-title trapezoid">Blog</div>
        <nav>
            <ul>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/tkl-news' ) ); ?>">TKL News</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/innovations' ) ); ?>">Innovations</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/knowledge' ) ); ?>">Knowledge</a></li>
            </ul>
        </nav>
    </div>

    <main class="page-content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article class="container">
                <header class="single-article article-header">
                    <?php if(has_category()) { ?>
                        <div class="single-category">
                            <?php
                            $category = get_the_category();
                            echo $category[0]->cat_name;
                            ?>
                        </div>
                    <?php } ?>
                    <h2 class="single-title"><?php the_title(); ?></h2>
                    <div class="single-meta">
                        <span>by <?php the_author(); ?></span>
                        <span><?php the_time('F j, Y'); ?></span>
                    </div>
                </header>

                <div class="single-content article-content">
                    <?php get_template_part('template-parts/content', 'blog'); ?>
                </div>
            </article>

        <?php endwhile; ?>

            <div id="print-blog" class="container">
                <div class="share-bar no-print">
                    <ul>
                        <li id="print-me"><span class="print-button"><?php get_template_part('img/print', 'icon') ?>Print</span></li>

                        <?php if ( get_field( 'file_upload' ) ) { ?>
                            <li><a href="<?php the_field( 'file_upload' ); ?>" target="_blank" download><?php get_template_part('img/download', 'icon') ?>Download</a></li>
                        <?php } ?>

                        <li><a class="share-trigger"><?php get_template_part('img/share', 'icon') ?>Share</a></li>
                    </ul>

                    <?php get_template_part('template-parts/components/share', 'icons'); ?>
                </div>
            </div>

            <?php
            $next_post = get_next_post(true);
            if (!empty( $next_post )): ?>

                <div class="continue-reading">
                    <div class="container">
                        <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="cr-link">
                            <span class="link-text">
                                <div>Read Next</div>
                                <h4>
                                    <?php echo esc_attr( $next_post->post_title ); ?>
                                </h4>
                            </span>

                            <div class="cr-triangle"><img src="<?php echo esc_url( get_template_directory_uri()) . '/img/continue-triangle.svg';?>" alt="image description">
                            <span class="cr-arrow"><?php get_template_part('img/rarr'); ?></span>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endif; ?>

        <?php else: ?>

        <?php endif; ?>
    </main>
</div>



<?php get_footer(); ?>