<div class="search-field">
    <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="search-bar">
            <input type="search" value="" name="s" id="s" placeholder="Search" />
            <input type="submit" id="searchsubmit" value="Search" />
        </div>
    </form>
</div>