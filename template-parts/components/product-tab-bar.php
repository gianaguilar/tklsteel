<div id="tab-bar" class="tab-bar-menu">
    <div class="tab-bar-title trapezoid"><a href="<?php echo esc_url( home_url( '/steel-products' ) ); ?>">Steel Products</a></div>
    <nav>
        <span class="view-by">View By</span>
        <span class="tab-bar-btn type-btn">Product Type</span>
        <span class="tab-bar-btn usage-btn">Product Usage</span>
    </nav>
</div>

<?php get_template_part('template-parts/components/product', 'type' ); ?>

<?php get_template_part('template-parts/components/product', 'usage' ); ?>