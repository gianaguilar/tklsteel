<div class="usage-dropdown">
    <div class="menu-container">
        <div class="usage-dropdown-nav">
            <?php
                wp_nav_menu(array(
                    'theme_location' => 'menu-usage',
                    'menu_class' => 'usage-nav',
                    'container' => '',
                    'after' => '<span class="type-dropdown-trigger"></span>'
                ));
            ?>
        </div>
    </div>
</div>