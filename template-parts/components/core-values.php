<section class="home-values">
    <header class="slider-header">
        <h2 class="section-heading"><?php the_field( 'slider_title' ); ?></h3>
    </header><!-- /header -->
    <div id="core-values" class="flexslider">
        <ul class="slides">
            <?php if ( have_rows( 'slider' ) ) : while ( have_rows( 'slider' ) ) : the_row(); ?>
            <li>
                <div class="flex-slide">
                    <div class="flex-slide-content">
                        <h3 class="slide-title"><?php the_sub_field( 'slide_title' ); ?></h3>
                        <p class="slide-writeup"><?php the_sub_field( 'slide_text' ); ?></p>
                    </div>

                    <div class="flex-slide-image">
                        <?php if ( get_sub_field( 'slide_image' ) ) { ?>
                            <img class="slide-image" src="<?php the_sub_field( 'slide_image' ); ?>" />
                        <?php } ?>
                    </div>
                </div>
            </li>
            <?php endwhile; else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
        </ul>
    </div>
</section>