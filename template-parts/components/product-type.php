<div class="type-dropdown">
    <div class="menu-container">
        <div class="type-dropdown-nav">
            <?php
                wp_nav_menu(array(
                    'theme_location' => 'menu-types',
                    'menu_class' => 'type-nav',
                    'container' => '',
                    'after' => '<span class="type-dropdown-trigger"></span>'
                ));
            ?>
        </div>
    </div>
</div>