<header class="container">
    <h2><?php the_field( 'header_title' ); ?></h2>
    <div class="page-header">
        <div>
            <hr class="hr-bar-green">
            <p><?php the_field( 'header_text' ); ?></p>
        </div>
    </div>
</header>