<section class="home-values">
    <header class="slider-header">
        <h2 class="section-heading">More than <br>Just Steel</h3>
    </header><!-- /header -->
    <div id="core-values" class="flexslider">
        <ul class="slides">
            <li>
                <div class="flex-slide">
                    <div class="flex-slide-content">
                        <h3 class="slide-title">Consultative Team</h3>
                        <p class="slide-writeup">We don’t just sell, we can advise too. Our people are equipped with knowledge on our products  and where to use them. </p>
                    </div>

                    <div class="flex-slide-image">
                        <img class="slide-image" src="<?php echo esc_url( get_template_directory_uri()) . '/img/consultative-team.jpg';?>" alt="Consultative Team">
                    </div>
                </div>
            </li>

            <li>
                <div class="flex-slide">
                    <div class="flex-slide-content">
                        <h3 class="slide-title">Delivered Promises</h3>
                        <p class="slide-writeup">When we make an agreement, we follow through. Helping you accomplish your project is our primary motivation.</p>
                    </div>

                    <div class="flex-slide-image">
                        <img class="slide-image" src="<?php echo esc_url( get_template_directory_uri()) . '/img/delivered-promises.jpg';?>" alt="Delivered Promises">
                    </div>
                </div>
            </li>

            <li>
                <div class="flex-slide">
                    <div class="flex-slide-content">
                        <h3 class="slide-title">Competitive Price</h3>
                        <p class="slide-writeup">Our pricing is equivalent to our products quality. Because of our scale, we can price lower.</p>
                        <p>We do not overquote.</p>
                    </div>

                    <div class="flex-slide-image">
                        <img class="slide-image" src="<?php echo esc_url( get_template_directory_uri()) . '/img/competitive-price.jpg';?>" alt="Competitive Price">
                    </div>
                </div>
            </li>

            <li>
                <div class="flex-slide">
                    <div class="flex-slide-content">
                        <h3 class="slide-title">Superior Quality</h3>
                        <p class="slide-writeup">Your safety and reputation are our concern, which is why we make sure every piece of steel passes our company's quality standard.</p>
                    </div>

                    <div class="flex-slide-image">
                        <img class="slide-image" src="<?php echo esc_url( get_template_directory_uri()) . '/img/superior-quality.jpg';?>" alt="Superior Quality">
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>