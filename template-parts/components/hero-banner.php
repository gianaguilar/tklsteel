<header class="page-hero">
    <?php if (get_field('banner_type') == 'image') { ?>

        <div class="hero-banner">
            <img class="banner-image" src="<?php the_field( 'page_banner_image' ); ?>" />
            <a class="scroll-down" href="#target">Scroll Down</a>
        </div>

    <?php } else { ?>

        <div class="hero-banner">
            <ul class="slides">

                <?php if ( have_rows( 'banner_slider' ) ) : ?>
                    <?php while ( have_rows( 'banner_slider' ) ) : the_row(); ?>
                        <li>
                            <?php if ( get_sub_field( 'image' ) ) { ?>
                            <img class="banner-image" src="<?php the_sub_field( 'image' ); ?>" />
                            <?php } ?>
                        </li>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>

            </ul>

            <a class="scroll-down" href="#target">Scroll Down</a>
        </div>

    <?php } ?>

</header>