<header class="page-hero">
    <?php if (get_field('banner_type', 'cpt_product') == 'image') { ?>

        <div class="hero-banner">
            <img class="banner-image" src="<?php the_field( 'page_banner_image', 'cpt_product' ); ?>" />
            <a class="scroll-down" href="#target">Scroll Down</a>
        </div>

    <?php } else { ?>

        <div class="hero-banner">
            <ul class="slides">

                <?php if ( have_rows( 'banner_slider', 'cpt_product' ) ) : ?>
                    <?php while ( have_rows( 'banner_slider', 'cpt_product' ) ) : the_row(); ?>
                        <li>
                            <?php if ( get_sub_field( 'image', 'cpt_product' ) ) { ?>
                            <img class="banner-image" src="<?php the_sub_field( 'image', 'cpt_product' ); ?>" />
                            <?php } ?>
                        </li>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>

            </ul>

            <a class="scroll-down" href="#target">Scroll Down</a>
        </div>

    <?php } ?>

</header>