<div class="share-bar no-print">
    <ul>
        <li><span class="print-lightbox"><?php get_template_part('img/print', 'icon') ?>Print</span></li>

        <?php if ( get_field( 'file_upload' ) ) { ?>
            <li><a href="<?php the_field( 'file_upload' ); ?>" target="_blank" download><?php get_template_part('img/download', 'icon') ?>Download</a></li>
        <?php } ?>

        <li><a class="share-trigger"><?php get_template_part('img/share', 'icon') ?>Share</a></li>
    </ul>

    <?php get_template_part('template-parts/components/share', 'icons'); ?>
</div>