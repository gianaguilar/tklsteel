<div class="callout-container">
    <div class="chat-callout">
        <h3>Having trouble choosing the right steel?</h3>
        <p>Talk to us by clicking on the live chat button at the lower right corner of your screen.</p>
        <button class="close-callout"><img src="<?php echo esc_url( get_template_directory_uri()) . '/img/close-btn-green.svg';?>" alt="Close"></button>
    </div>
</div>