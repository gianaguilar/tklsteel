<div id="header" class="top-bar">
    <div class="top-bar-section top-bar-menu">
        <span class="icon-menu"></span>
    </div>

    <div class="top-bar-section top-bar-logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php get_template_part('img/tkl', 'logo'); ?></a>
    </div>

    <div class="top-bar-section top-bar-contact">
       <span class="icon-phone phone-popup-link" href="#phone-popup"><?php get_template_part('img/icon', 'phone'); ?></span><span class="call">Call</span><span class="number"><a href="tel:+6325881155" title="">(02) 588 - 1155 to 99</a></span>
    </div>
</div>

<!-- dialog itself, mfp-hide class is required to make dialog hidden -->
<div id="phone-popup" class="zoom-anim-dialog mfp-hide">
    <p><?php get_template_part('img/tkl', 'logo'); ?></p>

    <div class="number-container">
        <p><strong>Click on a number to call</strong></p>
        <p class="phone-numbers">
            <a href="tel:+6325881155">+63 (02) 588-1155</a>
            <a href="tel:+6325881156">+63 (02) 588-1156</a>
            <a href="tel:+6325881157">+63 (02) 588-1157</a>
            <a href="tel:+6325881158">+63 (02) 588-1158</a>
            <a href="tel:+6325881159">+63 (02) 588-1159</a>
            <a href="tel:+639189211056">+63 918 921 1056</a>
            <a href="tel:+639178627590">+63 917 862 7590</a>
            <a href="tel:+639228354585">+63 922 835 4585</a>
        </p>
    </div>
</div>