<div class="menu-overlay">
    <div class="menu-overlay-container">
        <header class="menu-overlay-header">
            <div class="menu-overlay-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/tkl-logo-rev-tagline.svg';?>" alt="image description" alt="TKL Steel Corp - Trust you can build on">
                </a>
            </div>
            <div class="search-bar-container">
                <?php get_template_part('searchform'); ?>
            </div>
        </header>
        <div class="menu-overlay-nav">
            <?php
                wp_nav_menu(array(
                    'theme_location' => 'menu-main',
                    'menu_class' => 'overlay-nav',
                    'container' => '',
                ));
            ?>
        </div>
        <footer class="menu-overlay-footer">
            <h5>Follow Us</h5>
            <ul class="social-links">
                <li><a href="https://twitter.com/tklsteelcorp" target="_blank"><?php get_template_part('img/icon', 'twitter'); ?></a></li>
                <li><a href="https://facebook.com/tklsteelcorp" target="_blank"><?php get_template_part('img/icon', 'facebook'); ?></a></li>
                <!-- <li><a href="#"><?php //get_template_part('img/icon', 'linkedin'); ?></a></li> -->
            </ul>
        </footer>
    </div>
</div>