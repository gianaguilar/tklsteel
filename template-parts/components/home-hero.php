<header class="hero" style="background-image: url('<?php the_field( 'background_image' ); ?>')">
    <div class="hero-content">
        <?php the_field( 'banner_title' ); ?>

        <?php if( get_field('banner_text') ): ?>
            <p><?php the_field( 'banner_text' ); ?></p>
        <?php endif; ?>

        <?php if( get_field('link_to') ): ?>
            <?php $link_to = get_field( 'link_to' ); if ( $link_to ): foreach ( $link_to as $post ): setup_postdata ( $post ); ?>
                <a class="btn" href="<?php the_permalink(); ?>">Learn More <?php get_template_part('img/rarr'); ?></a>
            <?php endforeach; wp_reset_postdata(); endif; ?>
        <?php endif; ?>
    </div>

    <a class="scroll-down" href="#target">Scroll Down</a>
</header>