<ul id="share-icons" class="isHidden">
    <li><a target="_blank" href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>"><?php get_template_part('img/icon', 'twitter'); ?></a></li>
    <li><a target="_blank" href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>"><?php get_template_part('img/icon', 'facebook'); ?></a></li>
    <li><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>&source=[SOURCE/DOMAIN]"><?php get_template_part('img/icon', 'linkedin'); ?></a></li>
    <li><a href="mailto:%20?subject=<?php print(urlencode(the_title())); ?>&body=<?php print(urlencode(get_permalink())); ?>" targe="_blank"><?php get_template_part('img/icon', 'mail'); ?></a></li>
    <li class="close-share"><img src="<?php echo esc_url( get_template_directory_uri()) . '/img/close-btn-green.svg';?>" alt="Close"></li>
</ul>