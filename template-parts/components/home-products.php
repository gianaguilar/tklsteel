<section class="home-products">
    <div class="home-products-header">
        <h3><?php the_field( 'product_title' ); ?></h3>
        <div>
            <p><?php the_field( 'product_text' ); ?></p>
            <?php $product_link_to = get_field( 'product_link_to' ); ?>
            <?php if ( $product_link_to ): ?>
                <?php foreach ( $product_link_to as $p ): ?>
                    <a class="btn" href="<?php echo get_permalink( $p ); ?>">View our products <?php get_template_part('img/rarr'); ?></a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="home-products-slider">
        <video class="video-desktop" autoplay muted loop playsinline>
            <source src="<?php echo esc_url( get_template_directory_uri()) . '/img/placeholder-slider.jpg';?>">
            <source src="<?php echo esc_url( get_template_directory_uri()) . '/videos/carousel.mp4';?>" type="video/mp4" />
        </video>

        <video class="video-mobile" autoplay muted loop playsinline>
            <source src="<?php echo esc_url( get_template_directory_uri()) . '/img/carousel-mobile.png';?>">
            <source src="<?php echo esc_url( get_template_directory_uri()) . '/videos/carousel-mobile.mp4';?>" type="video/mp4" />
        </video>
    </div>
</section>