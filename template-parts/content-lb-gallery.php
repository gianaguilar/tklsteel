<!-- SINGLE IMAGE -->
<?php if( get_field('image_gallery') == 'image' ): ?>
    <img class="lb-featured-image" src="<?php the_field( 'single_image' ); ?>" />
<?php endif; ?>


<!-- IMAGE GALLERY -->
<?php if( get_field('image_gallery') == 'gallery' ): ?>
    <?php $gallery_images = get_field( 'gallery' ); ?>
    <?php if ( $gallery_images ) :  ?>

    <section id="lb-gallery">
        <div class="lb-main-image">
            <img id="main-image" src="<?php echo $gallery_images[0][url]; ?>">
        </div>

        <div class="lb-gallery-thumbs">
            <ul class="lb-thumb-list">
                <?php foreach ( $gallery_images as $gallery_image ): ?>
                    <li>
                        <img class="thumb" src="<?php echo $gallery_image[url]; ?>" alt="<?php echo $gallery_image['caption']; ?>" />
                    </li>
                <?php endforeach; ?>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </section>

    <?php endif; ?>
<?php endif; ?>