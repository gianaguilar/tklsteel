<?php if ( have_rows( 'content_blocks' ) ): ?>
    <?php while ( have_rows( 'content_blocks' ) ) : the_row(); ?>

        <?php if ( get_row_layout() == 'normal_content' ) : ?>

            <section class="fc-block fc-content">
                <?php the_sub_field( 'content' ); ?>
            </section>


        <?php elseif ( get_row_layout() == 'blockquote' ) : ?>
            <section class="fc-block fc-blockquote">
                <blockquote><?php the_sub_field( 'quote' ); ?></blockquote>
            </section>

        <?php elseif ( get_row_layout() == 'image_block' ) : ?>
            <?php $image = get_sub_field( 'image' ); ?>

            <?php if ( $image ) { ?>
                <section class="fc-block fc-img-block">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['caption']; ?>" />
                </section>
            <?php } ?>


        <?php elseif ( get_row_layout() == 'image_gallery' ) : ?>
            <?php $gallery_images = get_sub_field( 'gallery' ); ?>
            <?php if ( $gallery_images ) :  ?>

                <section class="fc-block fc-gallery">

                        <div class="main-image-box">
                            <img id="main-image" src="<?php echo $gallery_images[0][url]; ?>" alt="Caption 0">
                        </div>

                        <div class="thumb-box">
                            <div class="thumb-list">
                                <?php foreach ( $gallery_images as $gallery_image ): ?>

                                    <img class="thumb" src="<?php echo $gallery_image[url]; ?>" alt="<?php echo $gallery_image['caption']; ?>" />

                                <?php endforeach; ?>
                            </div>

                            <div class="image-caption">
                                <hr class="hr-bar-green">
                                <p class="caption"><?php echo $gallery_images[0]['caption']; ?></p>
                            </div>
                        </div>

                </section>

            <?php endif; ?>
        <?php endif; ?>
    <?php endwhile; ?>

<?php else: ?>
    <?php // no layouts found ?>
<?php endif; ?>