<?php
/*
  search.php
  Our WordPress search results template
*/
get_header();

// http://website.com/?s=toronto+canada
$term = (isset($_GET['s'])) ? $_GET['s'] : ''; // Get 's' querystring param
?>
<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
</div>

<main class="page-content">
    <section class="container">
        <header id="search-header">
            <div class="searched-for">
                <span>You searched for</span>
                <h2> <?php echo esc_html( get_search_query( false ) ); ?> </h2>
            </div>

            <?php get_template_part('searchform'); ?>
        </header><!-- /header -->

        <div class="search-results">
            <?php
              echo do_shortcode('[ajax_load_more id="relevanssi" search="'. $term .'" repeater="template_6" preloaded="true" post_type="post, page, blog-posts, job, product, people, csr-project, client-story" posts_per_page="10" scroll="false" transition="fade" button_label="Load More Results" button_loading_label="Loading Results"]');
            ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>