 jQuery(document).ready(function($){

    // ================================================================
    // Flexslider
    // ================================================================

    $('#core-values').flexslider({
        animation: "slide",
        controlNav: false,
        startAt: 0,
        slideshow: false,
        slideshowSpeed: 9000,
    });

    $('#our-people-flexslider').flexslider({
        animation: "slide",
        controlNav: false,
    });

    $('.hero-banner').flexslider({
        animation: "slide",
        controlNav: true,
        directionNav: false,
    });


    // ================================================================
    // Menu Overlay
    // ================================================================

    $('.icon-menu').click(function() {
        $('#site-wrapper').toggleClass('menu-open');
        $('body').toggleClass('menu-open');
    });



    // ================================================================
    // Product Type Overlay
    // ================================================================

    $('.type-btn').click(function() {
        $('.type-dropdown').toggleClass('type-dropdown-isOpen');
        $('.type-btn').toggleClass('type-btn-isClicked');

        $('.usage-dropdown').removeClass('usage-dropdown-isOpen');
        $('.usage-btn').removeClass('usage-btn-isClicked');

        $('body').removeClass('usage-isopen');
        $('body').toggleClass('type-isopen');
    });



    // ================================================================
    // Product usage Overlay
    // ================================================================

    $('.usage-btn').click(function() {
        $('.usage-dropdown').toggleClass('usage-dropdown-isOpen');
        $('.usage-btn').toggleClass('usage-btn-isClicked');

        $('.type-dropdown').removeClass('type-dropdown-isOpen');
        $('.type-btn').removeClass('type-btn-isClicked');

        $('body').removeClass('type-isopen');
        $('body').toggleClass('usage-isopen');
    });





    // ================================================================
    // Smooth Scroll - https://codepen.io/gianaguilar/pen/YxeJVO
    // ================================================================

    $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
        // On-page links
        if ( location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname ) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                        scrollTop: target.offset().top - 0 // Topbar Height
                    }, 300, function() {
                });
            }
        }
    });



    // ================================================================
    // Ajax Popup
    // ================================================================


    $('#site-wrapper').magnificPopup({
        delegate: 'a.lightbox-popup',
        type: 'ajax',
        alignTop: true,
        overflowY: 'scroll', // as we know that popup content is tall we set scroll overflow by default to avoid jump
    });



    // ================================================================
    // Close Callout
    // ================================================================

    $('.close-callout').click(function() {
        $('.chat-callout').addClass('isHidden');
    });




    // ================================================================
    // Phone Numbers Popup
    // ================================================================
    $('.phone-popup-link').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

    // ================================================================
    // Image Gallery
    // ================================================================
    $(document).on("click", ".thumb", function(){
        $("#main-image").attr("src", $(this).attr("src"));
    });


    // ================================================================
    // Share
    // ================================================================
    $(document).on("click", ".share-trigger", function(){
        $("#share-icons").toggleClass('isHidden');
    });

    $(document).on("click", ".close-share", function(){
        $("#share-icons").toggleClass('isHidden');
    });


    // ================================================================
    // Application Content
    // ================================================================
    $(document).on("click", ".apply-now-back", function(){
        $(".job-content").toggleClass('isHidden');
        $(".application-content").toggleClass('isHidden');
    });

    $(document).on("click", ".apply-now", function(){
        $(".job-content").toggleClass('isHidden');
        $(".application-content").toggleClass('isHidden');
    });


    // ================================================================
    // Print - https://stackoverflow.com/questions/9344306/jquery-click-doesnt-work-on-ajax-generated-content
    // ================================================================
    $(document).on("click", ".print-lightbox", function(){
        $('.lightbox-post').print();
    });

    $('#print-blog .print-button').click(function(){
         $('.page-content').print();
    });



    // ================================================================
    // Align Products
    // ================================================================
    $('.products-index .alm-listing').append('<div class="filler"></div><div class="filler"></div><div class="filler"></div><div class="filler"></div>');

    // ================================================================
    // Product Dropdown
    // ================================================================

    $('.menu-item-has-children').click(function() {
        $('.menu-item-has-children').not(this).removeClass('isOpen');
        $(this).toggleClass('isOpen');
    });

});


// VANILA JS


// ================================================================
// Scroll Reveal
// ================================================================

window.sr = new ScrollReveal();
sr.reveal('.callout-container', {
    duration: 500,
    delay: 300,
    // mobile: false,
    origin: 'left',
    distance:'20%',
    scale: 1
});


// ================================================================
// Headroom
// ================================================================

(function() {
    var header = new Headroom(document.querySelector("#header"), {
        tolerance: 5,
        offset : 200,
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
    });
    header.init();

    var tbHeadroom = new Headroom(document.getElementById("tab-bar"), {
        tolerance : 0,
        offset : 200,
        classes : {
            initial : "slide",
            pinned : "tab-bar-pinned",
            unpinned : "tab-bar-unpinned"
        }
    });
    tbHeadroom.init();
}());

