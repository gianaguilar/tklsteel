<?php
/*
    Template Name: Contact
*/
get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
    <?php get_template_part('template-parts/components/hero', 'banner'); ?>
</div>

<main id="target" class="page-content">
    <div class="page-content-tab trapezoid">Contact Us</div>

    <section class="container">
        <header class="contact-header">
            <div>
                <h2>How can we help?</h2>
            </div>
        </header>


        <div class="contact-content">
            <div class="contact-form">
                <?php the_field( 'form_shortcode' ); ?>
            </div>

            <div class="contact-map">
                <div class="contact-map-container">
                    <iframe src="https://snazzymaps.com/embed/14784" width="100%" height="391px" style="border:none;"></iframe>
                </div>

                <br>

                <a href="http://bit.ly/2ie23mt" target="_blank" class="btn">View Larger Map</a>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>