<?php
/*
    Template Name: Blog
*/
get_header(); ?>


<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>

    <header class="page-hero">
        <div class="hero-banner">
        <ul class="slides">
            <?php
                // WP_Query arguments
                $args = array(
                    'post_type' => 'blog-posts',
                    'posts_per_page' => 3,
                );

                // The Query
                $query_blog = new WP_Query( $args );

                // The Loop
                if ( $query_blog->have_posts() ) {
                    while ( $query_blog->have_posts() ) {
                        $query_blog->the_post(); ?>

                        <li class="full-bg blog-slide" style="background-image: url('<?php the_post_thumbnail_url(); ?> ')" alt="slider image">

                            <div class="slide-article">
                                <header class="folder-label <?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>">
                                    <h3><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></h3>
                                </header>


                                <div class="slide-details">
                                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    <a class="btn-link" href="<?php the_permalink(); ?>">Read Story <?php get_template_part('img/rarr'); ?></a>
                                </div>
                            </div>
                        </li>

                    <?php }
                } else { ?>
                    <p>No posts found.</p>
                <?php }
                // Restore original Post Data
                wp_reset_postdata();
            ?>

        </ul>
                </div>
    </header>
</div>


<div class="contain-tabbar">
    <div id="tab-bar" class="tab-bar-menu">
        <div class="tab-bar-title trapezoid">Blog</div>
        <nav>
            <ul>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/tkl-news' ) ); ?>">TKL News</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/innovations' ) ); ?>">Innovations</a></li>
                <li><a class="tab-bar-link" href="<?php echo esc_url( home_url( '/category/knowledge' ) ); ?>">Knowledge</a></li>
            </ul>
        </nav>
    </div>

    <main class="page-content blog-content">
        <section class="container">
            <div class="blog-section tkl-news">
                <header class="folder-label">
                    <h3>TKL News</h3>
                </header>

                <?php
                    // WP_Query arguments
                    $args = array(
                        'post_type' => 'blog-posts',
                        'category_name' => 'tkl-news',
                        'posts_per_page' => 3,
                    );

                    // The Query
                    $query_news = new WP_Query( $args );

                    // The Loop
                    if ( $query_news->have_posts() ) {
                        while ( $query_news->have_posts() ) {
                            $query_news->the_post(); ?>

                            <article class="blog-excerpt">
                                <div class="excerpt-image">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>

                                            <?php the_post_thumbnail(); ?>

                                        <?php } else { ?>

                                            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/4x3.svg';?>" alt="No Image Found">

                                        <?php } ?>
                                    </a>


                                </div>
                                <div class="excerpt-details">
                                    <h4 class="excerpt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                    <div>
                                        <a class="btn btn-gray" href="<?php the_permalink(); ?>">Read Story <?php get_template_part('img/rarr'); ?></a>
                                    </div>
                                </div>
                            </article>

                        <?php }
                    } else { ?>
                        <p>No posts found.</p>
                    <?php }
                    // Restore original Post Data
                    wp_reset_postdata();
                ?>


                <div class="text-center">
                    <a class="view-more-btn" href="<?php echo esc_url( home_url( '/category/tkl-news' ) ); ?>">View More TKL News</a>
                </div>
            </div>

            <div class="blog-section innovations">
                <header class="folder-label">
                    <h3>Innovations</h3>
                </header>

                <?php
                    // WP_Query arguments
                    $args = array(
                        'post_type' => 'blog-posts',
                        'category_name' => 'innovations',
                        'posts_per_page' => 3
                    );

                    // The Query
                    $query_innovations = new WP_Query( $args );

                    // The Loop
                    if ( $query_innovations->have_posts() ) {
                        while ( $query_innovations->have_posts() ) {
                            $query_innovations->the_post(); ?>

                            <article class="blog-excerpt">
                                <div class="excerpt-image">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>

                                            <?php the_post_thumbnail(); ?>

                                        <?php } else { ?>

                                            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/4x3.svg';?>" alt="No Image Found">

                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="excerpt-details">
                                    <h4 class="excerpt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                    <div>
                                        <a class="btn btn-gray" href="<?php the_permalink(); ?>">Read Story <?php get_template_part('img/rarr'); ?></a>
                                    </div>
                                </div>
                            </article>

                        <?php }
                    } else { ?>
                        <p>No posts found.</p>
                    <?php }
                    // Restore original Post Data
                    wp_reset_postdata();
                ?>

                <div class="text-center">
                    <a class="view-more-btn" href="<?php echo esc_url( home_url( '/category/innovations' ) ); ?>">View More Innovations</a>
                </div>
            </div>

            <div class="blog-section knowledge">
                <header class="folder-label">
                    <h3>Knowledge</h3>
                </header>

                <?php
                    // WP_Query arguments
                    $args = array(
                        'post_type' => 'blog-posts',
                        'category_name' => 'knowledge',
                        'posts_per_page' => 3
                    );

                    // The Query
                    $query_knowledge = new WP_Query( $args );

                    // The Loop
                    if ( $query_knowledge->have_posts() ) {
                        while ( $query_knowledge->have_posts() ) {
                            $query_knowledge->the_post(); ?>

                            <article class="blog-excerpt">
                                <div class="excerpt-image">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php if ( has_post_thumbnail() ) { ?>

                                            <?php the_post_thumbnail(); ?>

                                        <?php } else { ?>

                                            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/4x3.svg';?>" alt="No Image Found">

                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="excerpt-details">
                                    <h4 class="excerpt-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                    <div>
                                        <a class="btn btn-gray" href="<?php the_permalink(); ?>">Read Story <?php get_template_part('img/rarr'); ?></a>
                                    </div>
                                </div>
                            </article>

                        <?php }
                    } else { ?>
                        <p>No posts found.</p>
                    <?php }
                    // Restore original Post Data
                    wp_reset_postdata();
                ?>


                <div class="text-center">
                    <a class="view-more-btn" href="<?php echo esc_url( home_url( '/category/knowledge' ) ); ?>">View More Knowledge</a>
                </div>
            </div>
        </section>
    </main>
</div>

<?php get_footer(); ?>