<?php
/*
    Template Name: Home
*/
get_header(); ?>

    <div class="contain-topbar">
        <?php get_template_part('template-parts/components/top', 'bar'); ?>
        <?php get_template_part('template-parts/components/home', 'hero'); ?>
    </div>

    <div id="target">

        <?php get_template_part('template-parts/components/core', 'values'); ?>

        <?php get_template_part('template-parts/components/home', 'products'); ?>

        <?php get_template_part('template-parts/components/callout'); ?>

    </div>

<?php get_footer(); ?>