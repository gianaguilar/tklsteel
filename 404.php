<?php get_header(); ?>

<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
</div>

<main class="page-404">
    <h2>404</h2>
    <h5>Page not found.</h5>
    <p><a class="btn" href="<?php echo esc_url( home_url( '/' ) ); ?>">Back to Home</a></p>
</main>


<?php get_footer(); ?>