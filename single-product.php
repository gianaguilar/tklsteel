<?php get_header(); ?>


<div class="contain-topbar">
    <?php get_template_part('template-parts/components/top', 'bar'); ?>
</div>

<div class="contain-tabbar">
    <?php get_template_part('template-parts/components/product', 'tab-bar'); ?>

    <main class="page-content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- post -->
        <section class="container">
            <header class="product-name-header">
                <h2 class="product-name"><?php the_title(); ?></h2>
            </header>

            <div class="product-page-content">
                <header>
                    <div>
                        <?php if ( has_post_thumbnail() ) { ?>

                            <?php the_post_thumbnail(); ?>

                        <?php } else { ?>

                            <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/4x3.svg';?>" alt="No Image Found">

                        <?php } ?>
                    </div>
                    <div>
                        <?php the_field( 'product_description' ); ?>
                    </div>
                </header><!-- /header -->

                <div class="product-details">
                    <header class="product-section-title">
                        <h3>Product Details</h3>
                    </header><!-- /header -->

                    <?php if ( have_rows( 'product_details' ) ) : ?>
                        <?php while ( have_rows( 'product_details' ) ) : the_row(); ?>

                        <div class="product-info">
                            <?php if( get_sub_field('product_type') ): ?>
                            <div class="info">
                                <h6>Product Type</h6>
                                <p><?php the_sub_field( 'product_type' ); ?></p>
                            </div>
                            <?php endif; ?>

                            <?php if( get_sub_field('product_code') ): ?>
                            <div class="info">
                                <h6>Product Code</h6>
                                <p><?php the_sub_field( 'product_code' ); ?></p>
                            </div>
                            <?php endif; ?>

                            <?php if( get_sub_field('product_application') ): ?>
                            <div class="info">
                                <h6>Product Application</h6>
                                <p><?php the_sub_field( 'product_application' ); ?></p>
                            </div>
                            <?php endif; ?>

                            <?php if ( get_sub_field( 'spec_sheet' ) ) { ?>
                            <div class="info">
                                <h6>Spec Sheet</h6>
                                <a href="<?php the_sub_field( 'spec_sheet' ); ?>" class="download-btn btn">Download</a>
                            </div>
                            <?php } ?>
                        </div>

                        <?php endwhile; ?>
                    <?php endif; ?>

                    <div class="buying-tips">
                        <?php if ( have_rows( 'buying_tips' ) ) : ?>
                            <?php while ( have_rows( 'buying_tips' ) ) : the_row(); ?>

                                <div class="tip">
                                    <div class="tip-gravatar">
                                        <div class="gravatar-image">
                                            <?php if ( get_sub_field( 'image' ) ) { ?>
                                                <img src="<?php the_sub_field( 'image' ); ?>" />
                                            <?php } else { ?>
                                                <img src="<?php echo esc_url( get_template_directory_uri()) . '/img/1x1.svg';?>">
                                            <?php } ?>
                                        </div>
                                        <div class="tip-info">
                                            <p class="tip-author"><?php the_sub_field( 'author' ); ?></p>
                                            <p class="tip-position"><?php the_sub_field( 'position' ); ?></p>
                                        </div>
                                    </div>

                                    <div class="tip-content">
                                        <h6><?php the_sub_field( 'tip_title' ); ?></h6>
                                        <p><?php the_sub_field( 'tip_content' ); ?></p>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>

                    </div>
                </div>

                <div class="related-products">
                    <header class="product-section-title">
                        <h3>Products you might be interested in</h3>
                        <a class="more-related-btn" href="<?php echo esc_url( home_url( '/steel-products' ) ); ?>">View More</a>
                    </header><!-- /header -->

                    <div class="related-products-index">

                        <?php $products = get_field( 'products' ); ?>
                        <?php if ( $products ): ?>
                            <?php foreach ( $products as $post ):  ?>
                                <?php setup_postdata ( $post ); ?>
                                    <a href="<?php the_permalink(); ?>" class="product">
                                        <?php if ( has_post_thumbnail() ) { ?>

                                            <img class="product-image" src="<?php the_post_thumbnail_url(); ?>">

                                        <?php } else { ?>

                                            <img class="product-image" src="<?php echo esc_url( get_template_directory_uri()) . '/img/4x3.svg';?>">

                                        <?php } ?>

                                        <h3 class="product-name"><?php the_title(); ?></h3>
                                    </a>
                            <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                        <?php endif; ?>

                        <div class="filler"></div>
                        <div class="filler"></div>
                        <div class="filler"></div>
                        <div class="filler"></div>
                    </div><!-- /.products-index -->
                </div>
            </div>
        </section>
        <?php endwhile; ?>
        <!-- post navigation -->
        <?php else: ?>
        <!-- no posts found -->
        <?php endif; ?>

        <?php get_template_part('template-parts/components/callout'); ?>
    </main>
</div>

<?php get_footer(); ?>